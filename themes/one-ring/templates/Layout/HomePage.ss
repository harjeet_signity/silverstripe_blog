<!-- BEGIN HOME SLIDER SECTION -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators 
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    </ol> -->

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
	<% loop SliderItems %>
	<div class="item <% if First %>active<% end_if %>" id="slide$Pos" style="background: url($BackgroundImage.Url) no-repeat left center; background-size: cover;"> 
	    <div class="carousel-caption">
		<div class="caption sfr slider-title">$Heading</div>
		<div class="caption sfl slider-subtitle">$SubHeading</div>
		<a href="#" class="caption sfb btn btn-default btn-lg">Learn More</a>
	    </div>
	</div>
	<% end_loop %>

    </div>
    <!-- Blue Filter -->
    <div id="home-search-section"></div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	<span class="glyphicon glyphicon-chevron-left"></span>
	<span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	<span class="glyphicon glyphicon-chevron-right"></span>
	<span class="sr-only">Next</span>
    </a>



</div>		
<!-- END HOME SLIDER SECTION -->

<!-- BEGIN HOME ADVANCED SEARCH -->
<!-----    
	    <div id="home-advanced-search" class="open">
		    <div id="opensearch"></div>
		    <div class="container">
			    <div class="row">
				    <div class="col-sm-12">
					    <form>
						    <div class="form-group">
							    <div class="form-control-small">
								    <div class='input-group date chzn-container' data-datepicker>
									    <input placeholder="Arrive on..." type='text' class="form-control" data-date-format="DD/MM/YYYY"/>
									    <span class="input-group-addon">
										    <span class="glyphicon glyphicon-calendar"></span>
									    </span>
								    </div>
							    </div>
							    
							    <div class="form-control-small">
								    <select id="search_status" name="search_status" data-placeholder="Stay...">
									    <option value=""> </option>
									    <option value="1">1 Night</option>
									    <option value="2">2 Nights</option>
									    <option value="3">3 Nights</option>
									    <option value="4">4 Nights</option>
									    <option value="5">5 Nights</option>
									    <option value="6">6 Nights</option>
									    <option value="7">7 Nights</option>
									    <option value="8">8 Nights</option>
									    <option value="9">9 Nights</option>
									    <option value="10">10 Nights</option>
									    <option value="11">11 Nights</option>
									    <option value="12">12 Nights</option>
									    <option value="13">13 Nights</option>
									    <option value="14">14 Nights</option>
								    </select>
							    </div>
							    
							    <div class="form-control-small">
								    <select id="search_bedrooms" name="search_bedrooms" data-placeholder="Bedrooms">
									    <option value=""> </option>
									    <option value="0">0</option>
									    <option value="1">1</option>
									    <option value="2">2</option>
									    <option value="3">3</option>
									    <option value="4">4</option>
									    <option value="5">5</option>
									    <option value="5plus">5+</option>
								    </select>
							    </div>
							    <div class="form-control-large">
								    <input type="text" class="form-control" name="location" placeholder="City, State, Country, etc...">
							    </div>
							    <button type="submit" class="btn btn-fullcolor">Search</button>
						    </div>
					    </form>

				    </div>
			    </div>
		    </div>
	    </div>
--->
<!-- END HOME ADVANCED SEARCH -->

<!-- BEGIN CONTENT WRAPPER -->
<div class="content">
    <div class="container">
	<div class="row">

	    <!-- BEGIN MAIN CONTENT -->
	    <div class="main col-sm-8">
		<h1 class="section-title">Portfolio</h1>

		<div class="grid-style1 clearfix">
		    <% loop $FeaturedProperties %>
		    <div class="item col-md-4">
                <div class="image">
                    <a href="$Link">
                    <h3>$Title</h3>
                   
                    </a>
                    $PrimaryPhoto.CroppedImage(220,194)
                </div>
                <div><h3>$Title</h3></div>
            </div>
          
		    <% end_loop %>

		</div>

	

		<h1 class="section-title">Recent Blog</h1>
		<div class="grid-style1">

		    <% loop $LatestBlogs(3) %>
		    <div class="item col-md-4">
			<div class="image">
			    <a href="$Link">
				<span class="btn btn-default"><i class="fa fa-file-o"></i> Read More</span>
			    </a>
			    $Photo.CroppedImage(220,148)
			</div>

			<div class="info-blog">
			    <ul class="top-info">
				<li><i class="fa fa-calendar"></i> $Date.Format('j F, Y')</li>
				<li><i class="fa fa-comments-o"></i> $Comment_count</li>
				<li><i class="fa fa-tags"></i>$CategoriesList</li>
			    </ul>
			    <h3>
				<a href="$Link">$Title</a>
			    </h3>
			    <p>$Content.FirstSentence</p>
			</div>
		    </div>
		    <% end_loop %>

		</div>
		<div style="clear:both;"></div>
		<div class="center"><a href="$BlogLink" class="btn btn-default-color">View All News</a></div>
	    </div>
	    <!-- END MAIN CONTENT -->

	    <!-- BEGIN SIDEBAR -->
	    <div class="sidebar col-sm-4">

		

		<div class="col-sm-12">
		    <h2 class="section-title">Activity</h2>
		    <ul class="activity">
			<li class="col-lg-12">
			    <a href="#"><img src="http://placehold.it/70x70" alt="" /></a>
			    <div class="info">										
				<h5>Sam Minnée reviewed <a href="#">The House With No Windows</a></h4>
				    <p>Awesome solitary confinement, mate. Spot on. Sweet as.</p>
				    <h6>Just now</h6>
			    </div>
			</li>
			<li class="col-lg-12">
			    <a href="#"><img src="http://placehold.it/70x70" alt="" /></a>
			    <div class="info">
				<h5>Ingo Schoomer asked a question about <a href="#">The Mistake by the Lake</a></h4>
				    <p>Has this house been unit tested?</p>
				    <h6>37 minutes ago</h6>
			    </div>
			</li>
		    </ul>
		</div>



	    </div>
	    <!-- END SIDEBAR -->

	</div>
    </div>
</div>
<!-- END CONTENT WRAPPER -->
