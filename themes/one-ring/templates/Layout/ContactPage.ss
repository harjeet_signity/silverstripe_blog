<div class="content contact-page">
    <div id="map"></div>
    <script>
	function initMap() {
	    var uluru = {lat: 30.7239758, lng: 76.8467309};
	    var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 20,
		center: uluru
	    });
	    var marker = new google.maps.Marker({
		position: uluru,
		map: map
	    });
	}
      </script>
      <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6KfNxPJoOSL2X9Rkq3gMp1hpehp-TwTk&callback=initMap">
      </script>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="well well-sm">
		    $ContactForm
                </div>
            </div>
            <div class="col-md-4">
                <form>
                    <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
                    <address>
                        <strong>Address</strong><br>
                        $Address                        
                    </address>
		    <address>
                        <strong>Phone</strong><br>
                        <abbr title="Phone">
                            P:</abbr>
                        $Phone_No
                    </address>
                    <address>
                        <strong>Email</strong><br>
                        <a href="mailto:$Email">$Email</a>
                    </address>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
