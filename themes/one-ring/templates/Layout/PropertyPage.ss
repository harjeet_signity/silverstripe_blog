<% include BreadCrumb %>
<!-- BEGIN CONTENT WRAPPER -->
<div class="content">
	<div class="container">
		<div class="row">
		
			<!-- BEGIN MAIN CONTENT -->
			<div class="main col-sm-12">
			
				<!-- BEGIN PROPERTY LISTING -->
			
					<% if $Results %>
						<% loop $Results %>
						<div class="item col-md-4">
							<div class="image">
								<a href="javascript:;">
									<span class="btn btn-default"><i class="fa fa-file-o"></i> Details</span>
								</a>
								$PrimaryPhoto.CroppedImage(300,200)
							</div>
              <!---
							<div class="price">
								<span>$PricePerNight.Nice</span><p>per night<p>
							</div>
              --->
							<div class="info">
								<h3>
									<a href="$Link">$Title</a>
									<small>$Region.Title</small>
								
								</h3>
								<p>$Description.LimitCharacters(100)</p>
							 <!---
								<ul class="amenities">
									<li><i class="icon-bedrooms"></i> $Bedrooms</li>
									<li><i class="icon-bathrooms"></i> $Bathrooms</li>
								</ul>
               ---->
							</div>
						</div>
						<% end_loop %>
					<% else %>
						<h3>There are no properties that match your search11.
					<% end_if %>
					
				<!-- END PROPERTY LISTING -->
				
				
				<!-- BEGIN PAGINATION -->
        
        <% if $Results.MoreThanOnePage %>
          <div class="pagination">
            <% if $Results.NotFirstPage %>
              <ul id="previous col-xs-6">
                  <li><a href="$Results.PrevLink"><i class="fa fa-chevron-left"></i></a></li>
              </ul>
            <% end_if %>
            <ul class="hidden-xs">
                <% loop $Results.PaginationSummary %>
                  <li <% if $CurrentBool %>class="active"<% end_if %>><a href="$Link">$PageNum</a></li>
                <% end_loop %>
            </ul>
            
             <% if $Results.NotLastPage %>
              <ul id="next col-xs-6">
                  <li><a href="$Results.NextLink"><i class="fa fa-chevron-right"></i></a></li>
              </ul>
            <% end_if %>
          </div>
        <% end_if %>
				<!-- END PAGINATION -->
        
        
				
			</div>	
			<!-- END MAIN CONTENT -->
			
			
		
		</div>
	</div>
</div>
<!-- END CONTENT WRAPPER -->
