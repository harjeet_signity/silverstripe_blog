<?php
class PropertyPage extends Page {
    /*
    private static $has_many = array (
      'Property' => 'Property',
    );
   */
   
  private static $has_many = array (
		'Comments' => 'PropertyComment'
	);
    
   private static $many_many = array (
        'Category' => 'Category'
    );

}

class PropertyPage_Controller extends Page_Controller {
   
  private static $allowed_actions = array (
    'detail',
    'CommentForm',
    'comment_submit',
    'test'
  );
  
 
  public function comment_submit(SS_HTTPRequest $request){
  
    $post = $request->postVars();
    $post = $request->postVars(); 
    $comment = PropertyComment::create();
    $comment->Name = $post['Name'];
    $comment->Email = $post['Email'];
    $comment->Comment = $post['Comment'];
    $comment->PropertyPageID = $post['property_detail_id'];
    $comment->write();
    $this->Sessioncomment();
    return $this->redirectBack();
  }
  
  function Sessioncomment() {
     Session::clear('ActionStatus'); 
     return Session::set('ActionStatus', 'Thanks for your comment');
   } 
  
  public function handleComment($data, $form) {
   
		Session::set("FormData.{$form->getName()}.data", $data);
		$existing = $this->Comments()->filter(array(
			'Comment' => $data['Comment']
		));		
		if($existing->exists() && strlen($data['Comment']) > 20) {
			$form->sessionMessage('That comment already exists! Spammer!','bad');

			return $this->redirectBack();
		}
		$comment = PropertyComment::create();
		$comment->PropertyPageID = $this->ID;
		$form->saveInto($comment);
		$comment->write();

		Session::clear("FormData.{$form->getName()}.data");
		$form->sessionMessage('Thanks for your comment','good');

		return $this->redirectBack();
	}
    
  public function index(SS_HTTPRequest $request) {
    
    //$properties = Property::get()->limit(20)->toArray();
     $properties = Property::get();
     
     
     $paginatedProperties = PaginatedList::create(
            $properties,
            $request
        )->setPageLength(6)->setPaginationGetVar('s');
     
    return array (
			'Results' => $paginatedProperties
		);
  }
  
  public function detail(SS_HTTPRequest $request) {
     //$stage = Versioned::current_stage();
     
     $form = Form::create(
			$this,
			'comment_submit',
			FieldList::create(
				TextField::create('Name',''),
				EmailField::create('Email',''),
        HiddenField::create('property_detail_id', null,$request->param('ID')),
				TextareaField::create('Comment','')
			),
			FieldList::create(
				FormAction::create('handleComment','Post Comment')
					->setUseButtonTag(true)
					->addExtraClass('btn btn-default-color btn-lg')
			),
			RequiredFields::create('Name','Email','Comment')
		)->addExtraClass('form-style');

		foreach($form->Fields() as $field) {
			$field->addExtraClass('form-control')
				  ->setAttribute('placeholder', $field->getName().'*');
		}

		
    
     
    $comment_section  = PropertyComment::get()->filter(array(
        'PropertyPageID' => $request->param('ID')
     ));
     
     
     $property = Property::get()->byID($request->param('ID'));
     
     $latest_property   = Property::get()->limit(4);
     $list = ArrayList::create();
     $query = new SQLQuery(array ());
     $query->selectField("*")
              ->setFrom("Property")
              ->setOrderBy("Created", "ASC")
              ->setDistinct(true);

    $result = $query->execute();
    
    if($result) {
      while($record = $result->nextRecord()) {
        
        //echo $image = $record->FeaturedImage();
        
        $photo = Property::get("Image", "ParentID=".$record['PrimaryPhotoID']);
        
         $list->push(ArrayData::create(array(
                    'ID' => $record['ID'],
                    'Title' => $record['Title'],
                    'PricePerNight' => $record['PricePerNight'],
                    'Link' => $this->Link("detail/{$record['ID']}"),
                    'Bedrooms'=> $record['Bedrooms'],
                    'Bathrooms'=> $record['Bathrooms'],
                    'PrimaryPhotoID'=> $record['PrimaryPhotoID'],
                    'Description'=> $record['Description'],
                    'Created'=> $record['Created']
                    
                )));
      
      }
    }  

    
     
     return array (
        'Property' => $property,
        'latestpro' => $latest_property,
        'Comments' => $comment_section,
        'CommentForm1'  =>  $form
     );
  }
  
  
}

?>
