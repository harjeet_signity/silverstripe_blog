<?php
	class AboutPage extends Page {
		
        private static $has_many = array(
			'TeamMembers' => 'TeamMember'
		);
        
		public function getCMSFields() {
			$fields = parent::getCMSFields();
            
            $GridConfig = GridFieldConfig_RecordEditor::create();
			$Grid = new GridField("TeamMembers", "Team Members", $this->TeamMembers(), $GridConfig);
			$fields->addFieldToTab('Root.TeamMembers', $Grid);
			return $fields;
		}
	}

	class AboutPage_Controller extends Page_Controller {
    	
	}
