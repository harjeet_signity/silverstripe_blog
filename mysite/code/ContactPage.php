<?php

    class ContactPage extends Page {
        
        private static $db = array (
            'Phone_No' => 'Varchar(255)',
            'Address' => 'Varchar(255)',
            'Email' => 'Varchar(255)'
        );
        
        public function getCMSFields() {
			$fields = parent::getCMSFields();
            
            $fields->addFieldToTab('Root.Main', TextField::create('Phone_No','Phone Number'),'Content');
            $fields->addFieldToTab('Root.Main', TextField::create('Address'),'Content');
			$fields->addFieldToTab('Root.Main', TextField::create('Email','Email'), 'Content');
            
            $fields->removeByName('Content');
			return $fields;
		}
    }

    class ContactPage_Controller extends Page_Controller {
        
        private static $allowed_actions = array (
            'ContactForm',
        );
        
        public function ContactForm() {
            $form = Form::create(
                $this,
                __FUNCTION__,
                FieldList::create(
                    TextField::create('Name','')
                        ->setAttribute('placeholder','Enter Name*')
                        ->addExtraClass('form-control'),
                    EmailField::create('Email','')
                        ->setAttribute('placeholder','Enter Email*')
                        ->addExtraClass('form-control'),
                    NumericField::create('PhoneNo','')
                        ->setAttribute('placeholder','Enter Phoneno*')
                        ->addExtraClass('form-control'),
                    TextareaField::create('Message','')
                        ->setAttribute('placeholder','Enter Message*')
                        ->addExtraClass('form-control')
                ),
                FieldList::create(
                    FormAction::create('handleContactForm','Send Message')
                        ->setUseButtonTag(true)
                        ->addExtraClass('btn btn-primary pull-right')
                ),
                RequiredFields::create('Name','Email','PhoneNo', 'Message')
            );

            $form->addExtraClass('form-style');
            //pre($form);die;    
            return $form;
        }
        
        public function handleContactForm($data, $form) {
            $emailTo    =   "rommy@signitysolutions.com";
            $subject    =   "Contact Us query";
            $message    =   "<h3>You have recieved a query from following user.</h3>";
            $message    .=   "<p><b>Name : </b>{$data['Name']}<br>";
            $message    .=   "<b>Email : </b>{$data['Email']}<br>";
            $message    .=   "<b>Phone No : </b>{$data['PhoneNo']}<br>";
            $message    .=   "<b>Mesasge : </b>{$data['Message']}</p>";
            
            $email = new Email($data['Email'], $emailTo, $subject, $message);
            $response   =   $email->send();
            
            if($response){
                $form->sessionMessage('Thanks for your query!','alert alert-success');
            }else{
                $form->sessionMessage('Unable to send email.','alert alert-warning');
            }
            
            return $this->redirectBack();
        }
    }
