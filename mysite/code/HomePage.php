<?php
	class HomePage extends Page {
		
		private static $has_many = array(
			'SliderItems' => 'SliderItem'
		);
		
		public function getCMSFields() {
			$fields = parent::getCMSFields();

			$GridConfig = GridFieldConfig_RecordEditor::create();
			$Grid = new GridField("SliderItems", "Slider Items", $this->SliderItems(), $GridConfig);
			$fields->addFieldToTab('Root.Slider1', $Grid);
			
			return $fields;
		}
    
    public function BlogLink () {
        $page = BlogHolder::get()->first();

        if($page) {
            return $page->Link();
        }
    }
    
	}

	class HomePage_Controller extends Page_Controller {
    
    public function FeaturedProperties() {
      $property_arr  = Property::get()->filter(array('FeaturedOnHomepage' => true))->limit(6);
      return $property_arr;
    }
    
    public function LatestBlogs($count = 3) { 
      return BlogPage::get()
               ->sort('Created', 'DESC')
               ->limit($count);
    } 
       
		
	}
