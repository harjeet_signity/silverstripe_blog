<?php

	class TeamMember extends DataObject {

		private static $db = array(
			'Name' => 'Text',
			'Job_Title' => 'Text',
			'Description' => 'Varchar(255)'
		);

		private static $has_one = array(
			'BackgroundImage'   => 'Image',
            'AboutPage'         => 'AboutPage'
		);
		
		private static $summary_fields = array(
			'BackgroundImage.CMSThumbnail' => 'Thumbnail',
			'Name' => 'Name',
            'Job_Title' => 'Job_Title'
		);
		
		public function getCMSFields() {
			$fields = parent::getCMSFields();
			
			$fields = new FieldList(
				TextField::create('Name'),
				TextField::create('Job_Title'),
                TextareaField::create('Description'),
				$bgimage    =    UploadField::create('BackgroundImage')
			);
            
            $bgimage
                ->setFolderName('team-members')
                ->getValidator()->setAllowedExtensions(array('jpg','jpeg','gif','png'));
			
			return $fields;
		}

	}



