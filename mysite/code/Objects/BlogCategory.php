<?php
class BlogCategory extends DataObject {

    private static $db = array (
        'Title' => 'Varchar',
    );

    private static $has_one = array (
        'BlogHolder' => 'BlogHolder'
    );
    
    private static $belongs_many_many = array (
        'Blogs' => 'BlogPage'
    );

    public function getCMSFields() {
        return FieldList::create(
            TextField::create('Title')
        );
    }
    
    public function Link () {
		return $this->BlogHolder()->Link(
			'category/'.$this->ID
		);
	}
    
    public function LinkingMode() {
        
        return Controller::curr()->getRequest()->param('ID') == $this->ID ? 'current' : 'link';
    }
}
?>
