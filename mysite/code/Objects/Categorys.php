<?php
class Categorys extends DataObject {
   
   static $db = array(
      'Title' => 'Varchar'
   );
   
    private static $has_many = array(
        'Property' => 'Property'
    );
   
    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
      return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }
}
?>
