<?php
class Property extends DataObject {

    private static $db = array (
        'Title' => 'Varchar',
        'Description' => 'HTMLText',
        'PricePerNight' => 'Currency',
        'Bedrooms' => 'Int',
        'Bathrooms' => 'Int',
        'FeaturedOnHomepage' => 'Boolean'
    );

    private static $has_one = array (
        'HomePage' => 'HomePage',
        'PrimaryPhoto' => 'Image',
        'ButtonLink' => 'SiteTree',
        'SliderItem' => 'SliderItem',
        'PropertyPage' => 'PropertyPage',
        'Categorys' => 'Categorys',
        'Parent' => 'Category'
    );
    
    
    function getCMSFields_forPopup() {
      $fields = new FieldSet();
      $fields->push( new TextField('Name') );
      return $fields;
   }
    
    
    public function Link() {
        return $this->PropertyPage()->Link('profolio/detail/'.$this->ID);
    }
   

    public function getCMSfields() {
        
        $fields = FieldList::create(TabSet::create('Root'));
        
        $fields->addFieldsToTab('Root.Main', array(
            TextField::create('Title'),
            HtmlEditorField::create('Description','Description Detail'),
            CurrencyField::create('PricePerNight','Price (per night)'),
            DropdownField::create('Bedrooms')
                ->setSource(ArrayLib::valuekey(range(1,10))),
            DropdownField::create('Bathrooms')
                ->setSource(ArrayLib::valuekey(range(1,10))),
               
            DropdownField::create('SliderItemID','SliderItem')
                ->setSource(SliderItem::get()->map('ID','Heading')),
            
            TreedropdownField::create('ButtonLinkID', 'Link to', 'SiteTree'),
            
            CheckboxField::create('FeaturedOnHomepage','Feature on homepage')
        ));
        $fields->addFieldToTab('Root.Photos', $upload = UploadField::create(
            'PrimaryPhoto',
            'Primary photo'
        ));
        
        $upload->getValidator()->setAllowedExtensions(array(
            'png','jpeg','jpg','gif'
        ));
        $upload->setFolderName('property-photos');

        return $fields;
    }
}
?>
