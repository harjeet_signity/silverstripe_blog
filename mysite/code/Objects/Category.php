<?php
class Category extends DataObject {
   
   static $db = array(
      'Name' => 'Varchar'
   );
   
  static $has_one = array(
    'Parent' => 'Category'
  );

    static $has_many = array(
    'Children' => 'Category'
    );
   
   static $belongs_many_many = array(
      'PropertyPage' => 'PropertyPage'
   );
   
   function getCMSFields_forPopup() {
      $fields = new FieldSet();
      $fields->push( new TextField('Name') );
      return $fields;
   }
   
    public function canView($member = null) {
    
        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
     
      return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
    }
    
}
?>
