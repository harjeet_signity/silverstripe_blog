<?php

class BlogComment extends DataObject {

	private static $db = array (
		'Name' => 'Varchar',
		'Email' => 'Varchar',
		'Comment' => 'Text'
	);


	private static $has_one = array (
		'BlogPage' => 'BlogPage'
	);
  
   private static $summary_fields = array(
            'Created' => 'Created',
            'Name' => 'Name',
            'Email' => 'Email',
            'Comment' => 'Text'
        ); 
  
   public function getCMSFields(){
     
     
      $fields = FieldList::create(
          TextField::create('Name'),
          TextField::create('Email'),
          HtmlEditorField::create('Comment')
      );
      return $fields;
    }
}
