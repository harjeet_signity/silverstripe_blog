<?php

	class SliderItem extends DataObject {

		private static $db = array(
			'Heading' => 'Text',
			'SubHeading' => 'Varchar(255)'
		);

		private static $has_one = array(
			'BackgroundImage' => 'Image',
			'HomePage' => 'HomePage',
			'ButtonLink' => 'SiteTree'	
		);
		
		private static $summary_fields = array(
			'BackgroundImage.CMSThumbnail' => 'Thumbnail',
			'Heading' => 'Heading',
		);
		
		public function getCMSFields() {
			$fields = parent::getCMSFields();
			
			$fields = new FieldList(
				TextareaField::create('Heading'),
				TextField::create('SubHeading'),
				TreedropdownField::create('ButtonLinkID', 'Link to', 'SiteTree'),
				$bgimage = UploadField::create('BackgroundImage')
			);
			
            $bgimage
                ->setFolderName('slider-items')
                ->getValidator()->setAllowedExtensions(array('jpg','jpeg','gif','png'));
            
			return $fields;
		}

	}



