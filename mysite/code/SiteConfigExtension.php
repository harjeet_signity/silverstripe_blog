<?php

	class SiteConfigExtension extends DataExtension {

		private static $db = array (
			'FacebookLink' => 'Varchar',
			'TwitterLink' => 'Varchar',
			'GoogleLink' => 'Varchar',
			'YouTubeLink' => 'Varchar',
			'FooterContent' => 'Text',
			'Copyright' => 'Varchar'
		);
        
        private static $has_one = array(
            'Logo' => 'Image'
        );

		public function updateCMSFields(FieldList $fields) {
			$fields->addFieldsToTab('Root.Social', array (
				TextField::create('FacebookLink','Facebook'),
				TextField::create('TwitterLink','Twitter'),
				TextField::create('GoogleLink','Google'),
				TextField::create('YouTubeLink','YouTube')
			));
			$fields->addFieldsToTab('Root.Main', array(
                $logo = UploadField::create('Logo'),
				TextareaField::create('FooterContent', 'Content for footer'),
				TextField::create('Copyright', 'Content for Copyright')
			));
            
            $logo
                ->getValidator()->setAllowedExtensions(array('jpg','jpeg','gif','png'));
		}
	}
