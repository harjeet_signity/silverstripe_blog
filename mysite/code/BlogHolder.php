<?php
class BlogHolder extends Page {
  
  private static $has_many = array (
        'Categories' => 'BlogCategory'
    );
  
  private static $allowed_children = array (
		'BlogPage'
	);
  
   public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Categories', GridField::create(
            'Categories',
            'Blog categories',
            $this->Categories(),
            GridFieldConfig_RecordEditor::create()
        ));

        return $fields;
    }
    
  public function ArchiveDates() {
		$list = ArrayList::create();
		$stage = Versioned::current_stage();
    $query = new SQLQuery(array ());
		$query->selectField("DATE_FORMAT(`Date`,'%Y_%M_%m')","DateString")
			  ->setFrom("BlogPage_{$stage}")
			  ->setOrderBy("Date", "ASC")
        //->addWhere('Date != "NULL"')
			  ->setDistinct(true);
    //echo $query;
   
    $result = $query->execute();
    
   
    if($result) {
			while($record = $result->nextRecord()) {
        if(!empty($record['DateString'])) {
          list($year, $monthName, $monthNumber) = explode('_', $record['DateString']);
          $list->push(ArrayData::create(array(
            'Year' => $year,
            'MonthName' => $monthName,
            'MonthNumber' => $monthNumber,
            'Link' => $this->Link("date/$year/$monthNumber"),
            'ArticleCount' => BlogPage::get()->where("
                DATE_FORMAT(`Date`,'%Y%m') = '{$year}{$monthNumber}'
                AND ParentID = {$this->ID}
              ")->count()
          )));
        }
			}
		}
    return $list;
	}
  
  
  
  //==================== Blog search form =====================
  
  
    public function BlogSearchForm() {
       $form = Form::create(
            $this,
            'search',
            FieldList::create(
                TextField::create('Keywords','')
                    ->setAttribute('placeholder', 'Title...')
                    ->addExtraClass('form-control')
                           
            ),
            FieldList::create(
                FormAction::create('doblogSearch','Search')
                    ->addExtraClass('btn-lg btn-fullcolor')
            )
        );
        
        $form->setFormMethod('GET');

        return $form;
    }
    

}

class BlogHolder_Controller extends Page_Controller {
    
  private static $allowed_actions = array (
    'category',
    'region',
    'date',
    'search'
  );
  
  protected $articleList;

	public function init () {
      parent::init();
      /*
      $this->articleList = BlogPage::get()->filter(array(
        'ParentID' => $this->ID
      ))->sort('Date DESC')->toArray();
      */
       $this->articleList = BlogPage::get()->filter(array(
        'ParentID' => $this->ID
        ))->sort('ID DESC');
      
     /*
      foreach($this->articleList as $key=>$record){
        
        echo '<pre>';
          print_r($record->Title);
        echo '</pre>';
      }
      */ 
      
      
    }
    
    public function search(SS_HTTPRequest $request){
     $search = $request->getVar('Keywords');
      if($search = $request->getVar('Keywords')) {
       $this->articleList = $this->articleList->filter(array(
        'Title:PartialMatch' => $search
        ));
      }
      
      return array (
        'Selectedsearch' => $search
      );
    
    }
    
    public function category(SS_HTTPRequest $r) {
      $category = BlogCategory::get()->byID(
        $r->param('ID')
      );
          //echo $r->param('ID');
      if(!$category) {
        return $this->httpError(404,'That category was not found');
      }
    
      $this->articleList = $this->articleList->filter(array(
        'Categories.ID' => $category->ID
      ));
      

      return array (
        'SelectedCategory' => $category
      );
	  }
    
    
  public function date(SS_HTTPRequest $r) {
		$year = $r->param('ID');
		$month = $r->param('OtherID');

		if(!$year) return $this->httpError(404);

		$startDate = $month ? "{$year}-{$month}-01" : "{$year}-01-01";
		
		if(strtotime($startDate) === false) {
			return $this->httpError(404, 'Invalid date');
		} 

		$adder = $month ? '+1 month' : '+1 year';
		$endDate = date('Y-m-d', strtotime(
						$adder, 
						strtotime($startDate)
					));

		$this->articleList = $this->articleList->filter(array(
			'Date:GreaterThanOrEqual' => $startDate,
			'Date:LessThan' => $endDate 
		));

		return array (
			'StartDate' => DBField::create_field('SS_DateTime', $startDate),
			'EndDate' => DBField::create_field('SS_DateTime', $endDate)
		);

	}
    
    public function PaginatedArticles ($num = 3) {		
      return PaginatedList::create(
        $this->articleList,
        $this->getRequest()
      )->setPageLength($num);
    }

}
?>
