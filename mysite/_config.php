<?php
//Required:
define('SMTPMAILER_SMTP_SERVER_ADDRESS', 'smtp.gmail.com'); //SMTP server address
define('SMTPMAILER_DO_AUTHENTICATE', true); //Turn on SMTP server authentication. Set to false for an anonymous connection
define('SMTPMAILER_USERNAME', 'donotreplyonme@gmail.com'); //SMTP server username, if SMTPAUTH == true
define('SMTPMAILER_PASSWORD', 'signity@123'); //SMTP server password, if SMTPAUTH == true

//Optional:
define('SMTPMAILER_CHARSET_ENCODING', 'utf-8'); //Email characters encoding, e.g. : 'utf-8' or 'iso-8859-1'
define('SMTPMAILER_USE_SECURE_CONNECTION', 'ssl'); //SMTP encryption method : Set to '', 'tls', or 'ssl'
define('SMTPMAILER_SMTP_SERVER_PORT', 465); //SMTP server port. Set to 25 if no encryption is used, 465 if ssl or tls is activated
define('SMTPMAILER_DEBUG_MESSAGING_LEVEL', 0); //Print debugging informations. 0 = no debuging, 1 = print errors, 2 = print errors and messages, 4 = print full activity
define('SMTPMAILER_LANGUAGE_OF_MESSAGES', 'de'); //Language for messages. Look into smtp/code/vendor/language/ for available languages
define('SMTPMAILER_SEND_DELAY', 2000);//throttling, in milliseconds, can also be 0

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	'type' => 'MySQLDatabase',
	'server' => 'localhost',
	'username' => 'root',
	'password' => 'signity',
	'database' => 'silverstripe_blog',
	'path' => ''
);
Director::set_environment_type("dev");



//SS_Log::add_writer(new SS_LogFileWriter('silverstripe.log'), SS_Log::WARN, '>');
//Debug::log_errors_to("log/silverstripe.log");
//ini_set("log_errors", "On");
//ini_set("error_log", "log/silverstripe.log");

// Set the site locale
i18n::set_locale('en_US');
