-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 06, 2017 at 05:52 AM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `silverstripe_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `BlogCategory`
--

CREATE TABLE IF NOT EXISTS `BlogCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('BlogCategory') CHARACTER SET utf8 DEFAULT 'BlogCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `BlogHolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogHolderID` (`BlogHolderID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `BlogComment`
--

CREATE TABLE IF NOT EXISTS `BlogComment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('BlogComment') CHARACTER SET utf8 DEFAULT 'BlogComment',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Comment` mediumtext CHARACTER SET utf8,
  `BlogPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPageID` (`BlogPageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `BlogComment`
--

INSERT INTO `BlogComment` (`ID`, `ClassName`, `LastEdited`, `Created`, `Name`, `Email`, `Comment`, `BlogPageID`) VALUES
(1, 'BlogComment', '2017-04-05 07:54:10', '2017-04-05 07:54:10', 'sdfg', 'harjeet.s@signitysolutions.in', 'sdfg', 10);

-- --------------------------------------------------------

--
-- Table structure for table `BlogPage`
--

CREATE TABLE IF NOT EXISTS `BlogPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext CHARACTER SET utf8,
  `Author` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `BlogPage`
--

INSERT INTO `BlogPage` (`ID`, `Date`, `Teaser`, `Author`, `PhotoID`, `BrochureID`) VALUES
(10, '2017-04-05', 'Hello people. I am testing this blog page.', 'Harjeet Singh', 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `BlogPage_Categories`
--

CREATE TABLE IF NOT EXISTS `BlogPage_Categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPageID` int(11) NOT NULL DEFAULT '0',
  `BlogCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPageID` (`BlogPageID`),
  KEY `BlogCategoryID` (`BlogCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `BlogPage_Live`
--

CREATE TABLE IF NOT EXISTS `BlogPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext CHARACTER SET utf8,
  `Author` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `BlogPage_Live`
--

INSERT INTO `BlogPage_Live` (`ID`, `Date`, `Teaser`, `Author`, `PhotoID`, `BrochureID`) VALUES
(10, '2017-04-05', 'Hello people. I am testing this blog page.', 'Harjeet Singh', 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `BlogPage_versions`
--

CREATE TABLE IF NOT EXISTS `BlogPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext CHARACTER SET utf8,
  `Author` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `BlogPage_versions`
--

INSERT INTO `BlogPage_versions` (`ID`, `RecordID`, `Version`, `Date`, `Teaser`, `Author`, `PhotoID`, `BrochureID`) VALUES
(1, 10, 1, NULL, NULL, NULL, 0, 0),
(2, 10, 2, '2017-04-05', 'Hello people. I am testing this blog page.', 'Harjeet Singh', 0, 0),
(3, 10, 3, '2017-04-05', 'Hello people. I am testing this blog page.', 'Harjeet Singh', 16, 0),
(4, 10, 4, '2017-04-05', 'Hello people. I am testing this blog page.', 'Harjeet Singh', 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE IF NOT EXISTS `Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Category') CHARACTER SET utf8 DEFAULT 'Category',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Categorys`
--

CREATE TABLE IF NOT EXISTS `Categorys` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Categorys') CHARACTER SET utf8 DEFAULT 'Categorys',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ContactPage`
--

CREATE TABLE IF NOT EXISTS `ContactPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Phone No` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Phone_No` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ContactPage`
--

INSERT INTO `ContactPage` (`ID`, `Phone No`, `Address`, `Email`, `Phone_No`) VALUES
(3, NULL, '795 Folsom Ave, Suite 600 San Francisco, CA 94107', 'first.last@example.com', '01485-56235');

-- --------------------------------------------------------

--
-- Table structure for table `ContactPage_Live`
--

CREATE TABLE IF NOT EXISTS `ContactPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Phone No` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Phone_No` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ContactPage_Live`
--

INSERT INTO `ContactPage_Live` (`ID`, `Phone No`, `Address`, `Email`, `Phone_No`) VALUES
(3, NULL, '795 Folsom Ave, Suite 600 San Francisco, CA 94107', 'first.last@example.com', '01485-56235');

-- --------------------------------------------------------

--
-- Table structure for table `ContactPage_versions`
--

CREATE TABLE IF NOT EXISTS `ContactPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Phone No` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Phone_No` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ContactPage_versions`
--

INSERT INTO `ContactPage_versions` (`ID`, `RecordID`, `Version`, `Phone No`, `Address`, `Email`, `Phone_No`) VALUES
(1, 3, 3, '0', NULL, NULL, NULL),
(2, 3, 4, '0', '795 Folsom Ave, Suite 600 San Francisco, CA 94107', 'first.last@example.com', NULL),
(3, 3, 5, NULL, 'dgdfg', 'sdgdsfg', '0100410100'),
(4, 3, 6, NULL, '795 Folsom Ave, Suite 600 San Francisco, CA 94107', 'first.last@example.com', '01485-56235');

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage`
--

CREATE TABLE IF NOT EXISTS `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ErrorPage`
--

INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage_Live`
--

CREATE TABLE IF NOT EXISTS `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ErrorPage_Live`
--

INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage_versions`
--

CREATE TABLE IF NOT EXISTS `ErrorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ErrorPage_versions`
--

INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES
(1, 4, 1, 404),
(2, 5, 1, 500);

-- --------------------------------------------------------

--
-- Table structure for table `File`
--

CREATE TABLE IF NOT EXISTS `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('File','Folder','Image','Image_Cached') CHARACTER SET utf8 DEFAULT 'File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Filename` mediumtext CHARACTER SET utf8,
  `Content` mediumtext CHARACTER SET utf8,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `File`
--

INSERT INTO `File` (`ID`, `ClassName`, `LastEdited`, `Created`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `ParentID`, `OwnerID`) VALUES
(1, 'Folder', '2017-03-29 10:27:29', '2017-03-29 10:27:29', 'Uploads', 'Uploads', 'assets/Uploads/', NULL, 1, 0, 0),
(2, 'Image', '2017-03-29 10:27:29', '2017-03-29 10:27:29', 'SilverStripeLogo.png', 'SilverStripeLogo.png', 'assets/Uploads/SilverStripeLogo.png', NULL, 1, 1, 0),
(3, 'Image', '2017-03-29 13:30:02', '2017-03-29 13:30:02', 'home-bg-1.jpg', 'home bg 1', 'assets/Uploads/home-bg-1.jpg', NULL, 1, 1, 1),
(4, 'Image', '2017-03-29 13:30:40', '2017-03-29 13:30:40', 'booking-bg.jpg', 'booking bg', 'assets/Uploads/booking-bg.jpg', NULL, 1, 1, 1),
(5, 'Image', '2017-04-04 10:08:23', '2017-04-04 10:08:23', 'male-user-icon-32207.png', 'male user icon 32207', 'assets/Uploads/male-user-icon-32207.png', NULL, 1, 1, 1),
(6, 'Image', '2017-04-04 10:09:03', '2017-04-04 10:09:03', 'images.jpg', 'images', 'assets/Uploads/images.jpg', NULL, 1, 1, 1),
(7, 'Image', '2017-04-04 10:09:35', '2017-04-04 10:09:35', 'images-1.jpg', 'images 1', 'assets/Uploads/images-1.jpg', NULL, 1, 1, 1),
(8, 'Image', '2017-04-04 10:10:00', '2017-04-04 10:10:00', 'images-2.jpg', 'images 2', 'assets/Uploads/images-2.jpg', NULL, 1, 1, 1),
(9, 'Image', '2017-04-04 10:10:50', '2017-04-04 10:10:50', 'male-user-icon-32208.png', 'male user icon 32208', 'assets/Uploads/male-user-icon-32208.png', NULL, 1, 1, 1),
(12, 'Folder', '2017-04-04 11:02:47', '2017-04-04 11:02:47', 'team-members', 'team-members', 'assets/team-members/', NULL, 1, 0, 1),
(13, 'Image', '2017-04-04 11:02:48', '2017-04-04 11:02:48', 'f-img1-1.jpg', 'f img1 1', 'assets/team-members/f-img1-1.jpg', NULL, 1, 12, 1),
(14, 'Image', '2017-04-04 11:04:20', '2017-04-04 11:04:20', 'booking-bg.jpg', 'booking bg', 'assets/team-members/booking-bg.jpg', NULL, 1, 12, 1),
(15, 'Folder', '2017-04-05 07:55:06', '2017-04-05 07:55:06', 'travel-photos', 'travel-photos', 'assets/travel-photos/', NULL, 1, 0, 1),
(17, 'Folder', '2017-04-05 08:14:08', '2017-04-05 08:14:08', 'property-photos', 'property-photos', 'assets/property-photos/', NULL, 1, 0, 1),
(18, 'Image', '2017-04-05 08:14:08', '2017-04-05 08:14:08', 'booking-bg.jpg', 'booking bg', 'assets/property-photos/booking-bg.jpg', NULL, 1, 17, 1),
(19, 'Image', '2017-04-05 08:57:40', '2017-04-05 08:57:40', 'booking-bg.jpg', 'booking bg', 'assets/travel-photos/booking-bg.jpg', NULL, 1, 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Group`
--

CREATE TABLE IF NOT EXISTS `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Group') CHARACTER SET utf8 DEFAULT 'Group',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext CHARACTER SET utf8,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Group`
--

INSERT INTO `Group` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `ParentID`) VALUES
(1, 'Group', '2017-03-29 05:57:26', '2017-03-29 05:57:26', 'Content Authors', NULL, 'content-authors', 0, 1, NULL, 0),
(2, 'Group', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'Administrators', NULL, 'administrators', 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Group_Members`
--

CREATE TABLE IF NOT EXISTS `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Group_Members`
--

INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Group_Roles`
--

CREATE TABLE IF NOT EXISTS `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `LoginAttempt`
--

CREATE TABLE IF NOT EXISTS `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('LoginAttempt') CHARACTER SET utf8 DEFAULT 'LoginAttempt',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Status` enum('Success','Failure') CHARACTER SET utf8 DEFAULT 'Success',
  `IP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Member`
--

CREATE TABLE IF NOT EXISTS `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Member') CHARACTER SET utf8 DEFAULT 'Member',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `FirstName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `RememberLoginToken` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `NumVisit` int(11) NOT NULL DEFAULT '0',
  `LastVisited` datetime DEFAULT NULL,
  `AutoLoginHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `DateFormat` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `TimeFormat` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Email` (`Email`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Member`
--

INSERT INTO `Member` (`ID`, `ClassName`, `LastEdited`, `Created`, `FirstName`, `Surname`, `Email`, `TempIDHash`, `TempIDExpired`, `Password`, `RememberLoginToken`, `NumVisit`, `LastVisited`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `DateFormat`, `TimeFormat`) VALUES
(1, 'Member', '2017-04-05 07:25:06', '2017-03-29 05:57:29', 'Default Admin', NULL, 'admin', 'c1416d7ca7207cdf8f1d0689a6cc0e1a922bc9b4', '2017-04-08 07:25:06', '$2y$10$3224b301f02f257c643f1uk45tbObLmS7ExXYlqe1JKE.fyz9wiVS', NULL, 9, '2017-04-05 14:27:58', NULL, NULL, 'blowfish', '10$3224b301f02f257c643f15', NULL, NULL, 'en_US', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MemberPassword`
--

CREATE TABLE IF NOT EXISTS `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('MemberPassword') CHARACTER SET utf8 DEFAULT 'MemberPassword',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `MemberPassword`
--

INSERT INTO `MemberPassword` (`ID`, `ClassName`, `LastEdited`, `Created`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES
(1, 'MemberPassword', '2017-03-29 05:57:29', '2017-03-29 05:57:29', '$2y$10$3224b301f02f257c643f1uk45tbObLmS7ExXYlqe1JKE.fyz9wiVS', '10$3224b301f02f257c643f15', 'blowfish', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Permission`
--

CREATE TABLE IF NOT EXISTS `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Permission') CHARACTER SET utf8 DEFAULT 'Permission',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `Permission`
--

INSERT INTO `Permission` (`ID`, `ClassName`, `LastEdited`, `Created`, `Code`, `Arg`, `Type`, `GroupID`) VALUES
(1, 'Permission', '2017-03-29 05:57:26', '2017-03-29 05:57:26', 'CMS_ACCESS_CMSMain', 0, 1, 1),
(2, 'Permission', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'CMS_ACCESS_AssetAdmin', 0, 1, 1),
(3, 'Permission', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'CMS_ACCESS_ReportAdmin', 0, 1, 1),
(4, 'Permission', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'SITETREE_REORGANISE', 0, 1, 1),
(5, 'Permission', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'ADMIN', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionRole`
--

CREATE TABLE IF NOT EXISTS `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRole') CHARACTER SET utf8 DEFAULT 'PermissionRole',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionRoleCode`
--

CREATE TABLE IF NOT EXISTS `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRoleCode') CHARACTER SET utf8 DEFAULT 'PermissionRoleCode',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Property`
--

CREATE TABLE IF NOT EXISTS `Property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Property') CHARACTER SET utf8 DEFAULT 'Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HomePageID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `ButtonLinkID` int(11) NOT NULL DEFAULT '0',
  `SliderItemID` int(11) NOT NULL DEFAULT '0',
  `PropertyPageID` int(11) NOT NULL DEFAULT '0',
  `CategorysID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `HomePageID` (`HomePageID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`),
  KEY `ButtonLinkID` (`ButtonLinkID`),
  KEY `SliderItemID` (`SliderItemID`),
  KEY `PropertyPageID` (`PropertyPageID`),
  KEY `CategorysID` (`CategorysID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Property`
--

INSERT INTO `Property` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Description`, `PricePerNight`, `Bedrooms`, `Bathrooms`, `FeaturedOnHomepage`, `HomePageID`, `PrimaryPhotoID`, `ButtonLinkID`, `SliderItemID`, `PropertyPageID`, `CategorysID`, `ParentID`) VALUES
(1, 'Property', '2017-04-05 08:47:12', '2017-04-05 08:14:00', 'lkml,l', '<p>lkmkl</p>', 0.00, 1, 1, 1, 0, 18, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PropertyComment`
--

CREATE TABLE IF NOT EXISTS `PropertyComment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PropertyComment') CHARACTER SET utf8 DEFAULT 'PropertyComment',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Comment` mediumtext CHARACTER SET utf8,
  `PropertyPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PropertyPageID` (`PropertyPageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PropertyPage_Category`
--

CREATE TABLE IF NOT EXISTS `PropertyPage_Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PropertyPageID` int(11) NOT NULL DEFAULT '0',
  `CategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PropertyPageID` (`PropertyPageID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage`
--

CREATE TABLE IF NOT EXISTS `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage_Live`
--

CREATE TABLE IF NOT EXISTS `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage_versions`
--

CREATE TABLE IF NOT EXISTS `RedirectorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig`
--

CREATE TABLE IF NOT EXISTS `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteConfig') CHARACTER SET utf8 DEFAULT 'SiteConfig',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Tagline` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Theme` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `FacebookLink` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `TwitterLink` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `GoogleLink` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `YouTubeLink` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `FooterContent` mediumtext CHARACTER SET utf8,
  `Copyright` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `SiteConfig`
--

INSERT INTO `SiteConfig` (`ID`, `ClassName`, `LastEdited`, `Created`, `Title`, `Tagline`, `Theme`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `FacebookLink`, `TwitterLink`, `GoogleLink`, `YouTubeLink`, `FooterContent`, `Copyright`) VALUES
(1, 'SiteConfig', '2017-03-30 14:40:16', '2017-03-29 05:57:28', 'Your Site Name', 'your tagline here', 'one-ring', 'Anyone', 'LoggedInUsers', 'LoggedInUsers', 'http://facebook.com', 'http://twitter.com', 'http://google.com', 'http://youtube.com', 'This is some lerm ipsum content for footer. This is some lerm ipsum content for footer. This is some lerm ipsum content for footer. This is some lerm ipsum content for footer.\r\nThis is some lerm ipsum content for footer.This is some lerm ipsum content for footer.This is some lerm ipsum content for footer.', '2017 Team Signiity');

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_CreateTopLevelGroups`
--

CREATE TABLE IF NOT EXISTS `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_EditorGroups`
--

CREATE TABLE IF NOT EXISTS `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_ViewerGroups`
--

CREATE TABLE IF NOT EXISTS `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree`
--

CREATE TABLE IF NOT EXISTS `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteTree','Page','AboutPage','BlogHolder','BlogPage','ContactPage','HomePage','PropertyPage','ErrorPage','RedirectorPage','VirtualPage') CHARACTER SET utf8 DEFAULT 'SiteTree',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `SiteTree`
--

INSERT INTO `SiteTree` (`ID`, `ClassName`, `LastEdited`, `Created`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Version`, `ParentID`) VALUES
(1, 'HomePage', '2017-03-29 12:31:34', '2017-03-29 05:57:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(2, 'AboutPage', '2017-04-04 08:43:47', '2017-03-29 05:57:27', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(3, 'ContactPage', '2017-04-03 11:56:13', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 6, 0),
(4, 'ErrorPage', '2017-03-29 05:57:30', '2017-03-29 05:57:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(5, 'ErrorPage', '2017-03-29 05:57:30', '2017-03-29 05:57:29', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(6, 'Page', '2017-04-04 07:03:47', '2017-04-04 06:50:38', 'footer', 'Footer', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(7, 'Page', '2017-04-04 06:53:37', '2017-04-04 06:52:28', 'terms-and-conditions', 'Terms & Conditions', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt diam non ex efficitur, vitae facilisis erat lacinia. Maecenas non faucibus nunc. Mauris gravida pretium dolor ut aliquam. Quisque hendrerit neque vel nunc aliquam, id pulvinar nunc porta. Donec nibh libero, vestibulum nec condimentum id, molestie vitae est. Nunc vitae mollis ligula, a aliquet sapien. Aenean eget convallis purus. Suspendisse quis leo consectetur, scelerisque purus ac, malesuada ex.</p><p>Praesent sit amet nulla a leo molestie interdum sed vel lectus. Cras lobortis vitae arcu ut auctor. Maecenas et vestibulum orci, consectetur eleifend eros. Aliquam ut magna tempor eros dictum lacinia. Cras semper lacus justo, a posuere tellus tincidunt nec. Etiam justo urna, consequat eu mattis vel, lacinia in justo. Nullam ac lobortis massa, vel tincidunt magna. Donec id sagittis tortor. Donec viverra eget dui at facilisis. Donec at egestas urna. Aenean quis mollis mauris. Vestibulum ornare sem risus, eu imperdiet justo blandit ac.</p><p>Morbi laoreet luctus consequat. Fusce vestibulum aliquam sem. Phasellus vitae tincidunt est. Praesent in sem ut ipsum sodales semper ac sit amet est. Phasellus faucibus erat vel elementum sodales. Proin sollicitudin feugiat sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer posuere dolor venenatis eros consequat, ut congue urna tristique. Vestibulum cursus, est non hendrerit elementum, velit nibh tincidunt urna, commodo efficitur ex neque non erat. Quisque eget urna et velit semper tempus. Nunc nec lacinia diam, ornare pulvinar orci. Integer fringilla eu dui vitae convallis. Donec congue posuere rutrum.</p><p>Fusce congue mauris nec volutpat lobortis. Curabitur nec tellus dui. Aliquam pretium odio eget lacus congue, quis vulputate tortor faucibus. Nam et augue vestibulum, ullamcorper mauris eu, venenatis tortor. Nulla luctus bibendum massa, eget porta odio efficitur eu. Ut non neque nisi. Suspendisse ac ipsum auctor, dictum velit vel, condimentum lectus. Suspendisse sit amet fringilla enim. Quisque eu luctus lorem, at placerat dolor. Nunc condimentum lorem eu nibh luctus, nec tristique urna tincidunt. Donec sit amet porttitor tortor.</p><p> </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 2, 6),
(8, 'Page', '2017-04-04 06:54:16', '2017-04-04 06:53:54', 'privacy-policy', 'Privacy Policy', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt diam non ex efficitur, vitae facilisis erat lacinia. Maecenas non faucibus nunc. Mauris gravida pretium dolor ut aliquam. Quisque hendrerit neque vel nunc aliquam, id pulvinar nunc porta. Donec nibh libero, vestibulum nec condimentum id, molestie vitae est. Nunc vitae mollis ligula, a aliquet sapien. Aenean eget convallis purus. Suspendisse quis leo consectetur, scelerisque purus ac, malesuada ex.</p><p>Praesent sit amet nulla a leo molestie interdum sed vel lectus. Cras lobortis vitae arcu ut auctor. Maecenas et vestibulum orci, consectetur eleifend eros. Aliquam ut magna tempor eros dictum lacinia. Cras semper lacus justo, a posuere tellus tincidunt nec. Etiam justo urna, consequat eu mattis vel, lacinia in justo. Nullam ac lobortis massa, vel tincidunt magna. Donec id sagittis tortor. Donec viverra eget dui at facilisis. Donec at egestas urna. Aenean quis mollis mauris. Vestibulum ornare sem risus, eu imperdiet justo blandit ac.</p><p>Morbi laoreet luctus consequat. Fusce vestibulum aliquam sem. Phasellus vitae tincidunt est. Praesent in sem ut ipsum sodales semper ac sit amet est. Phasellus faucibus erat vel elementum sodales. Proin sollicitudin feugiat sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer posuere dolor venenatis eros consequat, ut congue urna tristique. Vestibulum cursus, est non hendrerit elementum, velit nibh tincidunt urna, commodo efficitur ex neque non erat. Quisque eget urna et velit semper tempus. Nunc nec lacinia diam, ornare pulvinar orci. Integer fringilla eu dui vitae convallis. Donec congue posuere rutrum.</p><p>Fusce congue mauris nec volutpat lobortis. Curabitur nec tellus dui. Aliquam pretium odio eget lacus congue, quis vulputate tortor faucibus. Nam et augue vestibulum, ullamcorper mauris eu, venenatis tortor. Nulla luctus bibendum massa, eget porta odio efficitur eu. Ut non neque nisi. Suspendisse ac ipsum auctor, dictum velit vel, condimentum lectus. Suspendisse sit amet fringilla enim. Quisque eu luctus lorem, at placerat dolor. Nunc condimentum lorem eu nibh luctus, nec tristique urna tincidunt. Donec sit amet porttitor tortor.</p><p> </p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 2, 6),
(9, 'BlogHolder', '2017-04-05 07:51:29', '2017-04-05 07:51:19', 'blogs', 'Blogs', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(10, 'BlogPage', '2017-04-05 09:05:20', '2017-04-05 07:52:25', 'test-blog-1', 'Test Blog 1', NULL, '<p>This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 4, 9),
(11, 'PropertyPage', '2017-04-05 08:20:55', '2017-04-05 08:20:43', 'services', 'Services', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_EditorGroups`
--

CREATE TABLE IF NOT EXISTS `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_ImageTracking`
--

CREATE TABLE IF NOT EXISTS `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `FileID` (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_LinkTracking`
--

CREATE TABLE IF NOT EXISTS `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_Live`
--

CREATE TABLE IF NOT EXISTS `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteTree','Page','AboutPage','BlogHolder','BlogPage','ContactPage','HomePage','PropertyPage','ErrorPage','RedirectorPage','VirtualPage') CHARACTER SET utf8 DEFAULT 'SiteTree',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `SiteTree_Live`
--

INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `LastEdited`, `Created`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Version`, `ParentID`) VALUES
(1, 'HomePage', '2017-03-29 12:31:34', '2017-03-29 05:57:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(2, 'AboutPage', '2017-04-04 08:43:47', '2017-03-29 05:57:27', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(3, 'ContactPage', '2017-04-03 11:56:13', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 6, 0),
(4, 'ErrorPage', '2017-03-29 05:57:34', '2017-03-29 05:57:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(5, 'ErrorPage', '2017-03-29 05:57:29', '2017-03-29 05:57:29', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(6, 'Page', '2017-04-04 07:03:47', '2017-04-04 06:50:38', 'footer', 'Footer', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(7, 'Page', '2017-04-04 06:53:37', '2017-04-04 06:52:28', 'terms-and-conditions', 'Terms & Conditions', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt diam non ex efficitur, vitae facilisis erat lacinia. Maecenas non faucibus nunc. Mauris gravida pretium dolor ut aliquam. Quisque hendrerit neque vel nunc aliquam, id pulvinar nunc porta. Donec nibh libero, vestibulum nec condimentum id, molestie vitae est. Nunc vitae mollis ligula, a aliquet sapien. Aenean eget convallis purus. Suspendisse quis leo consectetur, scelerisque purus ac, malesuada ex.</p><p>Praesent sit amet nulla a leo molestie interdum sed vel lectus. Cras lobortis vitae arcu ut auctor. Maecenas et vestibulum orci, consectetur eleifend eros. Aliquam ut magna tempor eros dictum lacinia. Cras semper lacus justo, a posuere tellus tincidunt nec. Etiam justo urna, consequat eu mattis vel, lacinia in justo. Nullam ac lobortis massa, vel tincidunt magna. Donec id sagittis tortor. Donec viverra eget dui at facilisis. Donec at egestas urna. Aenean quis mollis mauris. Vestibulum ornare sem risus, eu imperdiet justo blandit ac.</p><p>Morbi laoreet luctus consequat. Fusce vestibulum aliquam sem. Phasellus vitae tincidunt est. Praesent in sem ut ipsum sodales semper ac sit amet est. Phasellus faucibus erat vel elementum sodales. Proin sollicitudin feugiat sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer posuere dolor venenatis eros consequat, ut congue urna tristique. Vestibulum cursus, est non hendrerit elementum, velit nibh tincidunt urna, commodo efficitur ex neque non erat. Quisque eget urna et velit semper tempus. Nunc nec lacinia diam, ornare pulvinar orci. Integer fringilla eu dui vitae convallis. Donec congue posuere rutrum.</p><p>Fusce congue mauris nec volutpat lobortis. Curabitur nec tellus dui. Aliquam pretium odio eget lacus congue, quis vulputate tortor faucibus. Nam et augue vestibulum, ullamcorper mauris eu, venenatis tortor. Nulla luctus bibendum massa, eget porta odio efficitur eu. Ut non neque nisi. Suspendisse ac ipsum auctor, dictum velit vel, condimentum lectus. Suspendisse sit amet fringilla enim. Quisque eu luctus lorem, at placerat dolor. Nunc condimentum lorem eu nibh luctus, nec tristique urna tincidunt. Donec sit amet porttitor tortor.</p><p> </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 2, 6),
(8, 'Page', '2017-04-04 06:54:16', '2017-04-04 06:53:54', 'privacy-policy', 'Privacy Policy', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt diam non ex efficitur, vitae facilisis erat lacinia. Maecenas non faucibus nunc. Mauris gravida pretium dolor ut aliquam. Quisque hendrerit neque vel nunc aliquam, id pulvinar nunc porta. Donec nibh libero, vestibulum nec condimentum id, molestie vitae est. Nunc vitae mollis ligula, a aliquet sapien. Aenean eget convallis purus. Suspendisse quis leo consectetur, scelerisque purus ac, malesuada ex.</p><p>Praesent sit amet nulla a leo molestie interdum sed vel lectus. Cras lobortis vitae arcu ut auctor. Maecenas et vestibulum orci, consectetur eleifend eros. Aliquam ut magna tempor eros dictum lacinia. Cras semper lacus justo, a posuere tellus tincidunt nec. Etiam justo urna, consequat eu mattis vel, lacinia in justo. Nullam ac lobortis massa, vel tincidunt magna. Donec id sagittis tortor. Donec viverra eget dui at facilisis. Donec at egestas urna. Aenean quis mollis mauris. Vestibulum ornare sem risus, eu imperdiet justo blandit ac.</p><p>Morbi laoreet luctus consequat. Fusce vestibulum aliquam sem. Phasellus vitae tincidunt est. Praesent in sem ut ipsum sodales semper ac sit amet est. Phasellus faucibus erat vel elementum sodales. Proin sollicitudin feugiat sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer posuere dolor venenatis eros consequat, ut congue urna tristique. Vestibulum cursus, est non hendrerit elementum, velit nibh tincidunt urna, commodo efficitur ex neque non erat. Quisque eget urna et velit semper tempus. Nunc nec lacinia diam, ornare pulvinar orci. Integer fringilla eu dui vitae convallis. Donec congue posuere rutrum.</p><p>Fusce congue mauris nec volutpat lobortis. Curabitur nec tellus dui. Aliquam pretium odio eget lacus congue, quis vulputate tortor faucibus. Nam et augue vestibulum, ullamcorper mauris eu, venenatis tortor. Nulla luctus bibendum massa, eget porta odio efficitur eu. Ut non neque nisi. Suspendisse ac ipsum auctor, dictum velit vel, condimentum lectus. Suspendisse sit amet fringilla enim. Quisque eu luctus lorem, at placerat dolor. Nunc condimentum lorem eu nibh luctus, nec tristique urna tincidunt. Donec sit amet porttitor tortor.</p><p> </p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 2, 6),
(9, 'BlogHolder', '2017-04-05 07:51:30', '2017-04-05 07:51:19', 'blogs', 'Blogs', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0),
(10, 'BlogPage', '2017-04-05 08:57:41', '2017-04-05 07:52:25', 'test-blog-1', 'Test Blog 1', NULL, '<p>This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 4, 9),
(11, 'PropertyPage', '2017-04-05 08:20:55', '2017-04-05 08:20:43', 'services', 'Services', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_versions`
--

CREATE TABLE IF NOT EXISTS `SiteTree_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SiteTree','Page','AboutPage','BlogHolder','BlogPage','ContactPage','HomePage','PropertyPage','ErrorPage','RedirectorPage','VirtualPage') CHARACTER SET utf8 DEFAULT 'SiteTree',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `SiteTree_versions`
--

INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `LastEdited`, `Created`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `ParentID`) VALUES
(1, 1, 1, 1, 0, 0, 'Page', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(2, 2, 1, 1, 0, 0, 'Page', '2017-03-29 05:57:27', '2017-03-29 05:57:27', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(3, 3, 1, 1, 0, 0, 'Page', '2017-03-29 05:57:28', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(4, 4, 1, 1, 0, 0, 'ErrorPage', '2017-03-29 05:57:28', '2017-03-29 05:57:28', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(5, 5, 1, 1, 0, 0, 'ErrorPage', '2017-03-29 05:57:29', '2017-03-29 05:57:29', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(6, 1, 2, 1, 1, 1, 'HomePage', '2017-03-29 12:29:26', '2017-03-29 05:57:27', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>.</p><p>You can now access the <a href="http://docs.silverstripe.org">developer documentation</a>, or begin the <a href="http://www.silverstripe.org/learn/lessons">SilverStripe lessons</a>.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(7, 3, 2, 1, 1, 1, 'ContactPage', '2017-03-31 08:35:51', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(8, 3, 3, 1, 1, 1, 'ContactPage', '2017-04-03 11:03:40', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(9, 3, 4, 1, 1, 1, 'ContactPage', '2017-04-03 11:05:55', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(10, 3, 5, 1, 1, 1, 'ContactPage', '2017-04-03 11:10:09', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(11, 3, 6, 1, 1, 1, 'ContactPage', '2017-04-03 11:11:40', '2017-03-29 05:57:28', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(12, 6, 1, 0, 1, 0, 'Page', '2017-04-04 06:50:38', '2017-04-04 06:50:38', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(13, 6, 2, 0, 1, 0, 'Page', '2017-04-04 06:51:28', '2017-04-04 06:50:38', 'footer', 'Footer', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(14, 6, 3, 1, 1, 1, 'Page', '2017-04-04 06:51:53', '2017-04-04 06:50:38', 'footer', 'Footer', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(15, 7, 1, 0, 1, 0, 'Page', '2017-04-04 06:52:28', '2017-04-04 06:52:28', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(16, 7, 2, 1, 1, 1, 'Page', '2017-04-04 06:53:34', '2017-04-04 06:52:28', 'terms-and-conditions', 'Terms & Conditions', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt diam non ex efficitur, vitae facilisis erat lacinia. Maecenas non faucibus nunc. Mauris gravida pretium dolor ut aliquam. Quisque hendrerit neque vel nunc aliquam, id pulvinar nunc porta. Donec nibh libero, vestibulum nec condimentum id, molestie vitae est. Nunc vitae mollis ligula, a aliquet sapien. Aenean eget convallis purus. Suspendisse quis leo consectetur, scelerisque purus ac, malesuada ex.</p><p>Praesent sit amet nulla a leo molestie interdum sed vel lectus. Cras lobortis vitae arcu ut auctor. Maecenas et vestibulum orci, consectetur eleifend eros. Aliquam ut magna tempor eros dictum lacinia. Cras semper lacus justo, a posuere tellus tincidunt nec. Etiam justo urna, consequat eu mattis vel, lacinia in justo. Nullam ac lobortis massa, vel tincidunt magna. Donec id sagittis tortor. Donec viverra eget dui at facilisis. Donec at egestas urna. Aenean quis mollis mauris. Vestibulum ornare sem risus, eu imperdiet justo blandit ac.</p><p>Morbi laoreet luctus consequat. Fusce vestibulum aliquam sem. Phasellus vitae tincidunt est. Praesent in sem ut ipsum sodales semper ac sit amet est. Phasellus faucibus erat vel elementum sodales. Proin sollicitudin feugiat sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer posuere dolor venenatis eros consequat, ut congue urna tristique. Vestibulum cursus, est non hendrerit elementum, velit nibh tincidunt urna, commodo efficitur ex neque non erat. Quisque eget urna et velit semper tempus. Nunc nec lacinia diam, ornare pulvinar orci. Integer fringilla eu dui vitae convallis. Donec congue posuere rutrum.</p><p>Fusce congue mauris nec volutpat lobortis. Curabitur nec tellus dui. Aliquam pretium odio eget lacus congue, quis vulputate tortor faucibus. Nam et augue vestibulum, ullamcorper mauris eu, venenatis tortor. Nulla luctus bibendum massa, eget porta odio efficitur eu. Ut non neque nisi. Suspendisse ac ipsum auctor, dictum velit vel, condimentum lectus. Suspendisse sit amet fringilla enim. Quisque eu luctus lorem, at placerat dolor. Nunc condimentum lorem eu nibh luctus, nec tristique urna tincidunt. Donec sit amet porttitor tortor.</p><p> </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(17, 8, 1, 0, 1, 0, 'Page', '2017-04-04 06:53:54', '2017-04-04 06:53:54', 'new-page', 'New Page', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(18, 8, 2, 1, 1, 1, 'Page', '2017-04-04 06:54:14', '2017-04-04 06:53:54', 'privacy-policy', 'Privacy Policy', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt diam non ex efficitur, vitae facilisis erat lacinia. Maecenas non faucibus nunc. Mauris gravida pretium dolor ut aliquam. Quisque hendrerit neque vel nunc aliquam, id pulvinar nunc porta. Donec nibh libero, vestibulum nec condimentum id, molestie vitae est. Nunc vitae mollis ligula, a aliquet sapien. Aenean eget convallis purus. Suspendisse quis leo consectetur, scelerisque purus ac, malesuada ex.</p><p>Praesent sit amet nulla a leo molestie interdum sed vel lectus. Cras lobortis vitae arcu ut auctor. Maecenas et vestibulum orci, consectetur eleifend eros. Aliquam ut magna tempor eros dictum lacinia. Cras semper lacus justo, a posuere tellus tincidunt nec. Etiam justo urna, consequat eu mattis vel, lacinia in justo. Nullam ac lobortis massa, vel tincidunt magna. Donec id sagittis tortor. Donec viverra eget dui at facilisis. Donec at egestas urna. Aenean quis mollis mauris. Vestibulum ornare sem risus, eu imperdiet justo blandit ac.</p><p>Morbi laoreet luctus consequat. Fusce vestibulum aliquam sem. Phasellus vitae tincidunt est. Praesent in sem ut ipsum sodales semper ac sit amet est. Phasellus faucibus erat vel elementum sodales. Proin sollicitudin feugiat sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer posuere dolor venenatis eros consequat, ut congue urna tristique. Vestibulum cursus, est non hendrerit elementum, velit nibh tincidunt urna, commodo efficitur ex neque non erat. Quisque eget urna et velit semper tempus. Nunc nec lacinia diam, ornare pulvinar orci. Integer fringilla eu dui vitae convallis. Donec congue posuere rutrum.</p><p>Fusce congue mauris nec volutpat lobortis. Curabitur nec tellus dui. Aliquam pretium odio eget lacus congue, quis vulputate tortor faucibus. Nam et augue vestibulum, ullamcorper mauris eu, venenatis tortor. Nulla luctus bibendum massa, eget porta odio efficitur eu. Ut non neque nisi. Suspendisse ac ipsum auctor, dictum velit vel, condimentum lectus. Suspendisse sit amet fringilla enim. Quisque eu luctus lorem, at placerat dolor. Nunc condimentum lorem eu nibh luctus, nec tristique urna tincidunt. Donec sit amet porttitor tortor.</p><p> </p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(19, 6, 4, 1, 1, 1, 'Page', '2017-04-04 07:03:20', '2017-04-04 06:50:38', 'footer1', 'Footer', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(20, 6, 5, 1, 1, 1, 'Page', '2017-04-04 07:03:45', '2017-04-04 06:50:38', 'footer', 'Footer', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(21, 2, 2, 1, 1, 1, 'AboutPage', '2017-04-04 08:43:44', '2017-03-29 05:57:27', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(22, 9, 1, 0, 1, 0, 'BlogHolder', '2017-04-05 07:51:19', '2017-04-05 07:51:19', 'new-blog-holder', 'New Blog Holder', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(23, 9, 2, 1, 1, 1, 'BlogHolder', '2017-04-05 07:51:29', '2017-04-05 07:51:19', 'blogs', 'Blogs', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(24, 10, 1, 0, 1, 0, 'BlogPage', '2017-04-05 07:52:25', '2017-04-05 07:52:25', 'new-blog-page', 'New Blog Page', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 9),
(25, 10, 2, 1, 1, 1, 'BlogPage', '2017-04-05 07:53:33', '2017-04-05 07:52:25', 'test-blog-1', 'Test Blog 1', NULL, '<p>This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 9),
(26, 10, 3, 1, 1, 1, 'BlogPage', '2017-04-05 07:55:12', '2017-04-05 07:52:25', 'test-blog-1', 'Test Blog 1', NULL, '<p>This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 9),
(27, 11, 1, 0, 1, 0, 'PropertyPage', '2017-04-05 08:20:43', '2017-04-05 08:20:43', 'new-property-page', 'New Property Page', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(28, 11, 2, 1, 1, 1, 'PropertyPage', '2017-04-05 08:20:55', '2017-04-05 08:20:43', 'services', 'Services', NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(29, 10, 4, 1, 1, 1, 'BlogPage', '2017-04-05 08:57:41', '2017-04-05 07:52:25', 'test-blog-1', 'Test Blog 1', NULL, '<p>This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. This is demo content for blog 1. </p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 9);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_ViewerGroups`
--

CREATE TABLE IF NOT EXISTS `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SliderItem`
--

CREATE TABLE IF NOT EXISTS `SliderItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SliderItem') CHARACTER SET utf8 DEFAULT 'SliderItem',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Heading` mediumtext CHARACTER SET utf8,
  `SubHeading` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `HomePageID` int(11) NOT NULL DEFAULT '0',
  `BackgroundImageID` int(11) NOT NULL DEFAULT '0',
  `ButtonLinkID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `HomePageID` (`HomePageID`),
  KEY `ClassName` (`ClassName`),
  KEY `BackgroundImageID` (`BackgroundImageID`),
  KEY `ButtonLinkID` (`ButtonLinkID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `SliderItem`
--

INSERT INTO `SliderItem` (`ID`, `ClassName`, `LastEdited`, `Created`, `Heading`, `SubHeading`, `PhotoID`, `HomePageID`, `BackgroundImageID`, `ButtonLinkID`) VALUES
(1, 'SliderItem', '2017-03-29 14:15:26', '2017-03-29 13:30:06', 'Breathtaking views 111', 'Relaxation in the Bay of Belfalas 112', 0, 1, 3, 2),
(4, 'SliderItem', '2017-03-29 13:30:42', '2017-03-29 13:30:42', 'The simple life', 'Lush gardens in Mordor', 0, 1, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `TeamMember`
--

CREATE TABLE IF NOT EXISTS `TeamMember` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('TeamMember') CHARACTER SET utf8 DEFAULT 'TeamMember',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` mediumtext CHARACTER SET utf8,
  `Job_Title` mediumtext CHARACTER SET utf8,
  `Description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BackgroundImageID` int(11) NOT NULL DEFAULT '0',
  `AboutPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BackgroundImageID` (`BackgroundImageID`),
  KEY `ClassName` (`ClassName`),
  KEY `AboutPageID` (`AboutPageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `TeamMember`
--

INSERT INTO `TeamMember` (`ID`, `ClassName`, `LastEdited`, `Created`, `Name`, `Job_Title`, `Description`, `BackgroundImageID`, `AboutPageID`) VALUES
(1, 'TeamMember', '2017-04-04 10:08:28', '2017-04-04 10:08:28', 'Rakesh Singh', 'PHP Developer', 'lorm ipsum dummy text. lorm ipsum dummy text. lorm ipsum dummy text.', 5, 2),
(2, 'TeamMember', '2017-04-04 10:09:05', '2017-04-04 10:09:05', 'Amandeep Kaur', 'UI Designer', 'lorm ipsum dummy text. lorm ipsum dummy text. lorm ipsum dummy text.', 6, 2),
(3, 'TeamMember', '2017-04-04 10:09:37', '2017-04-04 10:09:37', 'Ajay Garg', 'Php Developer', 'lorm ipsum dummy text. lorm ipsum dummy text. lorm ipsum dummy text.', 7, 2),
(4, 'TeamMember', '2017-04-04 10:10:03', '2017-04-04 10:10:03', 'Mansi', 'Tester', 'lorm ipsum dummy text. lorm ipsum dummy text. lorm ipsum dummy text.', 8, 2),
(5, 'TeamMember', '2017-04-04 11:04:22', '2017-04-04 10:10:51', 'Vicky Jain', 'Android Developer', 'lorm ipsum dummy text. lorm ipsum dummy text. lorm ipsum dummy text.', 14, 2);

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage`
--

CREATE TABLE IF NOT EXISTS `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage_Live`
--

CREATE TABLE IF NOT EXISTS `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage_versions`
--

CREATE TABLE IF NOT EXISTS `VirtualPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
